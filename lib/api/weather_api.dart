import 'package:weather_forecast/api/api_key.dart';

class WeatherApi {
  final apiKey;
  WeatherApi({required this.apiKey});

  Uri _buildUri(String endpoint, Map<String, dynamic> queryParameter) {
    var query = {'appid': apiKey};
    if (queryParameter.isNotEmpty) {
      query = query..addAll(queryParameter);
    }

    Uri uri = Uri(
      scheme: 'http',
      host: 'api.openweathermap.org',
      path: 'data/2.5/$endpoint',
    );
    print('fetching $uri');
    return uri;
  }
}
